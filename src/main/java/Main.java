package main.java;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String inputFile = "Input.txt";
        String outputFile = "Output.txt";

        Scanner in = new Scanner(System.in);

        System.out.print("Click Enter if you want to encrypt or write 1 if you want to decrypt: ");
        String variant = in.nextLine();

        System.out.print("Enter key: ");
        String stringKey = in.nextLine();
        byte[] key = stringKey.getBytes();

        if (variant.equals("1")) {
            String input = inputFile;
            inputFile = outputFile;
            outputFile = input;
        }

        try {
            byte[] content = Files.readAllBytes(Paths.get(inputFile));

            for (int i = 0; i < content.length; i++) {
                content[i] = (byte) (content[i] ^ key[i % key.length]);
            }

            String result = new String(content);

            try {
                File file = new File(outputFile);
                FileWriter fileReader = new FileWriter(file);
                BufferedWriter bufferedWriter = new BufferedWriter(fileReader);
                bufferedWriter.write(result);
                bufferedWriter.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
